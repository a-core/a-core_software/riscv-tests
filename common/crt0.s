.section .init, "ax"
.global _start
_start:

    # Clear the bss segment
    la      a0, __bss_start
    la      a2, _end
    sub     a2, a2, a0
    li      a1, 0
    call    memset

    # Init stack and frame pointers
    la sp, __stack_top
    add s0, sp, zero

    jal zero, _init
