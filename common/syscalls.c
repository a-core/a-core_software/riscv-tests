// See LICENSE for license details.

#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <limits.h>
#include <sys/signal.h>
#include "dhrystone.h"
#include "util.h"
#include "a-core-utils.h"
#include "a-core.h"

#define SYS_write 64

#undef strcmp

extern volatile uint64_t tohost;
extern volatile uint64_t fromhost;
extern long User_Time;

// This function is probably not needed anywhere
static uintptr_t syscall(uintptr_t which, uint64_t arg0, uint64_t arg1, uint64_t arg2)
{
  volatile uint64_t magic_mem[8] __attribute__((aligned(64)));
  magic_mem[0] = which;
  magic_mem[1] = arg0;
  magic_mem[2] = arg1;
  magic_mem[3] = arg2;
  __sync_synchronize();

  tohost = (uintptr_t)magic_mem;
  while (fromhost == 0)
    ;
  fromhost = 0;

  __sync_synchronize();
  return magic_mem[0];
}

#define NUM_COUNTERS 2
static uintptr_t counters[NUM_COUNTERS];
static char* counter_names[NUM_COUNTERS];

void setStats(int enable)
{
  int i = 0;
#define READ_CTR(name) do { \
    while (i >= NUM_COUNTERS) ; \
    uintptr_t csr = read_csr(name); \
    if (!enable) { csr -= counters[i]; counter_names[i] = #name; } \
    counters[i++] = csr; \
  } while (0)

  READ_CTR(mcycle);
  READ_CTR(minstret);

#undef READ_CTR
}

void exit(int code)
{
    printf("Return code: %d\n\n", code);
    printf("HZ was %d\n", HZ);
    printf("User time was %d\n", User_Time);
    printf("Number_Of_Runs was %d\n", NUMBER_OF_RUNS);
    printf("\nCalculate the following by hand:\n");
    printf("Dhrystones_Per_Second = (HZ * Number_Of_Runs) / User_Time\n");
    printf("DMIPS = Dhrystones_Per_Second / 1757\n");
    printf("\n");
    if (code) 
    {
        test_fail();
    }
    else
    {
        test_pass();
    }
}

void _init(int cid, int nc)
{
    // init uart
    init_uart((volatile uint32_t*)A_CORE_AXI4LUART, BAUDRATE);
    int ret = main(0, 0);
    exit(ret);
}
