#=======================================================================
# Makefile for riscv-tests/isa
#-----------------------------------------------------------------------
# Copyright (c) 2012-2015, The Regents of the University of California (Regents).
# All Rights Reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the Regents nor the
#    names of its contributors may be used to endorse or promote products
#    derived from this software without specific prior written permission.
# 
# IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
# SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING
# OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF REGENTS HAS
# BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
# HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
# MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

XLEN ?= 32

src_dir := riscv-tests/isa

ifeq ($(XLEN),64)
include $(src_dir)/rv64ui/Makefrag
include $(src_dir)/rv64uc/Makefrag
include $(src_dir)/rv64um/Makefrag
include $(src_dir)/rv64ua/Makefrag
include $(src_dir)/rv64uf/Makefrag
include $(src_dir)/rv64ud/Makefrag
include $(src_dir)/rv64uzfh/Makefrag
include $(src_dir)/rv64si/Makefrag
include $(src_dir)/rv64ssvnapot/Makefrag
include $(src_dir)/rv64mi/Makefrag
include $(src_dir)/rv64mzicbo/Makefrag
endif
include $(src_dir)/rv32ui/Makefrag
include $(src_dir)/rv32uc/Makefrag
include $(src_dir)/rv32um/Makefrag
include $(src_dir)/rv32ua/Makefrag
include $(src_dir)/rv32uf/Makefrag
include $(src_dir)/rv32ud/Makefrag
include $(src_dir)/rv32uzfh/Makefrag
include $(src_dir)/rv32si/Makefrag
include $(src_dir)/rv32mi/Makefrag

bmarks = \
	median \
	qsort \
	rsort \
	towers \
	vvadd \
	memcpy \
	multiply \
	mm \
	dhrystone \
	spmv \
	mt-vvadd \
	mt-matmul \
	mt-memcpy \
	pmp \
	vec-memcpy \
	vec-daxpy \
	vec-sgemm \
	vec-strcmp \

default: all

#--------------------------------------------------------------------
# Build rules
#--------------------------------------------------------------------

RISCV_PREFIX ?= riscv64-unknown-elf-
RISCV_GCC ?= $(RISCV_PREFIX)gcc
RISCV_TESTS_GCC_OPTS ?= -march=$(MARCH) -mabi=$(MABI) -ffreestanding -nostartfiles -Os -fdata-sections -ffunction-sections --specs=nosys.specs -DFREQ_CLK_CORE=$(FREQ_CLK_CORE) -D$(PLATFORM)
RISCV_BMARK_GCC_OPTS ?= -march=$(MARCH) -mabi=$(MABI) -ffreestanding -nostartfiles -static -std=gnu99 -O2 -ffast-math -fno-common -fno-tree-loop-distribute-patterns --specs=nosys.specs -DFREQ_CLK_CORE=$(FREQ_CLK_CORE) -D$(PLATFORM)
RISCV_OBJDUMP ?= $(RISCV_PREFIX)objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data
RISCV_SIM ?= spike

# Program and data memory specifications
PROGMEM_START ?= 0x00001000
PROGMEM_LENGTH ?= 64K
DATAMEM_START ?= 0x20000000
DATAMEM_LENGTH ?= 64K

PLATFORM ?= sim
FREQ_CLK_CORE ?= 300000000

# A-Core generated header files
A_CORE_HEADERS_PATH ?= ../../../ACoreChip/chisel/include
A_CORE_HEADERS = $(wildcard $(A_CORE_HEADERS_PATH)/*.h)
INCLUDES = -I$(A_CORE_HEADERS_PATH) -Ienv_p_acore

# A-Core utility functions
A_CORE_LIB_PATH ?= ../a-core-library
A_CORE_UTILS_SOURCES = $(wildcard $(A_CORE_LIB_PATH)/a-core-utils/src/*.c)
A_CORE_UTILS_INCLUDE_PATH = $(A_CORE_LIB_PATH)/a-core-utils/include
A_CORE_UTILS_INCLUDES = $(wildcard $(A_CORE_UTILS_INCLUDE_PATH)/*.h)
A_CORE_LIB_INCLUDES = -I$(A_CORE_UTILS_INCLUDE_PATH) -I$(A_CORE_HEADERS_PATH)

DEFAULT_LINKER_PATH ?= ../a-core-library

STARTFILE ?= common/crt0.s

RISCV_TESTS_LDOPTS = -T env_p_acore/a-core.ld -lc_nano -lgcc -Wl,--gc-sections -Wl,--no-relax \
	-Wl,--defsym=PROGMEM_START=$(PROGMEM_START),--defsym=PROGMEM_LENGTH=$(PROGMEM_LENGTH),--defsym=DATAMEM_START=$(DATAMEM_START),--defsym=DATAMEM_LENGTH=$(DATAMEM_LENGTH) \
	-L$(DEFAULT_LINKER_PATH)

BMARK_LDOPTS = -T env_p_acore/a-core.ld -lc_nano -lm -Wl,--no-relax -static \
	-Wl,--defsym=PROGMEM_START=$(PROGMEM_START),--defsym=PROGMEM_LENGTH=$(PROGMEM_LENGTH),--defsym=DATAMEM_START=$(DATAMEM_START),--defsym=DATAMEM_LENGTH=$(DATAMEM_LENGTH) \
	-L$(DEFAULT_LINKER_PATH)

vpath %.S $(src_dir)

#------------------------------------------------------------
# Build assembly tests

# Saves the previous platform so that it know to recompile if it changes
.PHONY: force
platform_selection: force
	echo '$(PLATFORM)' | cmp -s - $@ || echo '$(PLATFORM)' > $@

%.dump: %
	$(RISCV_OBJDUMP) $< > $@

%.out: %
	$(RISCV_SIM) --isa=rv64gc_zfh_zicboz_svnapot_zicntr --misaligned $< 2> $@

%.out32: %
	$(RISCV_SIM) --isa=rv32gc_zfh_zicboz_svnapot_zicntr --misaligned $< 2> $@

a-core-utils.o: $(A_CORE_HEADERS) $(A_CORE_UTILS_SOURCES) $(A_CORE_UTILS_INCLUDES)
	$(RISCV_GCC) -c $(RISCV_TESTS_GCC_OPTS) $(A_CORE_UTILS_SOURCES) $(A_CORE_LIB_INCLUDES)

define compile_template

$$($(1)_p_tests): $(1)-p-%: $(1)/%.S
	$$(RISCV_GCC) $$(RISCV_TESTS_GCC_OPTS) $$< -Wall -o $$(PLATFORM).elf $$(RISCV_TESTS_LDOPTS) $$(INCLUDES) -I$(src_dir)/macros/scalar 
$(1)_tests += $$($(1)_p_tests)

# Not configured!
$$($(1)_v_tests): $(1)-v-%: $(1)/%.S
	$$(RISCV_GCC) $(2) $$(RISCV_GCC_OPTS) -DENTROPY=0x$$(shell echo \$$@ | md5sum | cut -c 1-7) -std=gnu99 -O2 -I$(src_dir)/../env/v -I$(src_dir)/macros/scalar -T$(src_dir)/../env/v/link.ld $(src_dir)/../env/v/entry.S $(src_dir)/../env/v/*.c $$< -o $$@
$(1)_tests += $$($(1)_v_tests)

$(1)_tests_dump = $$(addsuffix .dump, $$($(1)_tests))

$(1): $$($(1)_tests_dump)

.PHONY: $(1)

COMPILER_SUPPORTS_$(1) := $$(shell $$(RISCV_GCC) $(2) -c -x c /dev/null -o /dev/null 2> /dev/null; echo $$$$?)

ifeq ($$(COMPILER_SUPPORTS_$(1)),0)
tests += $$($(1)_tests)
endif

endef

bmark_src_dir = riscv-tests/benchmarks

.PHONY: dhrystone
dhrystone: $(wildcard $(bmark_src_dir)/dhrystone/*) $(A_CORE_HEADERS) $(wildcard ./common/*) $(STARTFILE) a-core-utils.o
	@echo "Changing one line in dhrystone.h"
	sed -i 's/#define HZ 1000000/#define HZ FREQ_CLK_CORE/g' $(bmark_src_dir)/dhrystone/dhrystone.h
	$(RISCV_GCC) $(INCLUDES) $(A_CORE_LIB_INCLUDES) -I$(bmark_src_dir)/dhrystone -I$(bmark_src_dir)/../env -I./common $(RISCV_BMARK_GCC_OPTS) -o $(PLATFORM).elf a-core-utils.o $(wildcard $(bmark_src_dir)/$@/*.c) $(wildcard $(bmark_src_dir)/$@/*.S) $(wildcard ./common/*.c) $(wildcard ./common/*.S) $(STARTFILE) $(BMARK_LDOPTS)

$(eval $(call compile_template,rv32ui,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32uc,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32um,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32ua,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32uf,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32ud,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32uzfh,-march=rv32g_zfh -mabi=ilp32))
$(eval $(call compile_template,rv32si,-march=rv32g -mabi=ilp32))
$(eval $(call compile_template,rv32mi,-march=rv32g -mabi=ilp32))
ifeq ($(XLEN),64)
$(eval $(call compile_template,rv64ui,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64uc,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64um,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64ua,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64uf,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64ud,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64uzfh,-march=rv64g_zfh -mabi=lp64))
$(eval $(call compile_template,rv64mzicbo,-march=rv64g_zicboz -mabi=lp64))
$(eval $(call compile_template,rv64si,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64ssvnapot,-march=rv64g -mabi=lp64))
$(eval $(call compile_template,rv64mi,-march=rv64g -mabi=lp64))
endif

tests_dump = $(addsuffix .dump, $(tests))
tests_hex = $(addsuffix .hex, $(tests))
tests_out = $(addsuffix .out, $(filter rv64%,$(tests)))
tests32_out = $(addsuffix .out32, $(filter rv32%,$(tests)))

run: $(tests_out) $(tests32_out)

junk += $(tests) $(tests_dump) $(tests_hex) $(tests_out) $(tests32_out)

#------------------------------------------------------------
# Default

all: $(RISCV_TEST_NAME)

#------------------------------------------------------------
# Clean up

clean:
	rm -rf $(junk)
